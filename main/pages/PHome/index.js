import React from 'react'
import { observer } from 'startupjs'
import { ScrollView, Image } from 'react-native'
import { Content, H2, Row, Div, Avatar, Span } from '@startupjs/ui'
import { Logo, AddSuggestionForm, SuggestionsList, Menu } from 'components'
import { RightSidebar } from 'main/components'
import './index.styl'

export default observer(function PHome () {
  return pug`
    ScrollView.root
      Image.bg(source={uri: 'img/background.png'})
      Content.content(width='wide')
        Row.row(vAlign='center' align='between')
          Logo.logo(width=200)
          Div.headerWrapper
            Menu.menu
            Row.user(vAlign='center' align='between')
              Div.nameWrapper
                Span.name(styleName=['greeting']) Hello
                Span.name(bold)  Mr. Cage!
              Avatar.avatar(src='https://www.placecage.com/c/200/200' size='xl')
        H2.title Bare it!
        Div.main
          Div.left
            AddSuggestionForm.form
            SuggestionsList
          RightSidebar.right
  `
})
