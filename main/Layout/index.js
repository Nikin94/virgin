import React from 'react'
import { observer, useValue } from 'startupjs'
import './index.styl'
import { Row, Div, Layout, SmartSidebar, Button, Span, Avatar } from '@startupjs/ui'
import { Logo, Menu, Socials } from 'components'
import { faBars } from '@fortawesome/free-solid-svg-icons'

export default observer(function ({ children }) {
  const [opened, $opened] = useValue(false)

  function renderSidebar () {
    return pug`
      Div.wrapper
        Logo.logo(width=182)
        Menu.menu(compact vertical)
        Socials.socials
    `
  }

  return pug`
    Layout
      SmartSidebar.sidebar(
        $open=$opened.path()
        renderContent=renderSidebar
      )
        Row.row(align='between' vAlign='center')
          Button.button(
            icon=faBars
            onPress=() => $opened.set(!opened)
          )
          Row.name(vAlign='center')
            Span Hello,
              Span(bold)  Mr. Cage!
            Avatar.avatar(src='https://www.placecage.com/c/200/200' size='s')
        Div.body
          = children
  `
})
