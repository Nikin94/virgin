import React from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { observer } from 'startupjs'
import { Events } from 'main/components'
import { Div, Span, Icon, Row } from '@startupjs/ui'
import { faBookOpen } from '@fortawesome/free-solid-svg-icons'
import { faMap, faCommentAlt } from '@fortawesome/free-regular-svg-icons'
import './index.styl'

export default observer(function RightSidebar ({ style }) {
  const items = [
    {
      icon: faBookOpen,
      title: 'Book of love'
    },
    {
      icon: faMap,
      title: 'Culture map'
    },
    {
      icon: faCommentAlt,
      title: 'Send feedback'
    }
  ]
  return pug`
    Div.root(style=style)
      Div.logo
        Image.image(source={uri: 'img/make-love-text.svg'})
        Span.mlsh #MLSH
      Row.menu(align='around')
        each item, index in items
          TouchableOpacity.item(key=index styleName={first: index === 0})
            Icon.icon(icon=item.icon size='xl')
            Span.title= item.title
      Events
  `
})
