import React, { useMemo } from 'react'
import { observer } from 'startupjs'
import { Div } from '@startupjs/ui'
import events from './events.json'
import TeammateEvents from './TeammateEvents'
import HotelEvents from './HotelEvents'
import './index.styl'

export default observer(function Events ({ style }) {
  const [teammateEvents, hotelEvents] = useMemo(() => {
    let _teammateEvents = []
    let _hotelEvents = []
    for (let event of events) {
      switch (event.type) {
        case 'teammate':
          _teammateEvents.push(event)
          break
        case 'hotel':
          _hotelEvents.push(event)
          break
      }
    }
    return [_teammateEvents, _hotelEvents]
  }, [JSON.stringify(events)])

  return pug`
    Div.root(style=style)
      TeammateEvents(events=teammateEvents)
      HotelEvents(events=hotelEvents)
  `
})
