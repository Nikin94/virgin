import React from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { Row, Span, Div, Button, Divider } from '@startupjs/ui'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import Title from '../Title'
import './index.styl'

export default observer(function TeammateEvents ({ style, events }) {
  const getDateString = date => {
    const { month, dayOfMonth, year, startTime } = date
    const monthCapitalized = month.replace(/^./, s => s.toUpperCase())
    return `${monthCapitalized} ${dayOfMonth}, ${year} at ${startTime}`
  }

  return pug`
    Div.root(style=style)
      Title(text='hotel')
      Div.events
        each event, index in events
          Row.event(
            key=index
            vAlign='center'
            align='between'
            styleName={first: index === 0}
          )
            Image.image(source={uri: event.image})
            Div.content
              Span.text(styleName=['title'])= event.title
              Span.text(styleName=['orgs'])= event.orgs.join(' + ')
              Span.text(styleName=['time'])= getDateString(event.date)
            Button.icon(
              variant='text'
              size='l'
              icon=faChevronDown
              onPress=() => {}
            )
          Divider.divider
  `
})
