import React from 'react'
import { observer } from 'startupjs'
import { Row, Span, Div, Button, Divider } from '@startupjs/ui'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import Title from '../Title'
import './index.styl'

export default observer(function TeammateEvents ({ style, events }) {
  return pug`
    Div.root(style=style)
      Title(text='teammate')
      Div.events
        each event, index in events
          Row.event(
            key=index
            vAlign='center'
            align='between'
            styleName={
              first: index === 0,
              last: index === events.length - 1
            }
          )
            Div.date
              Span.day(styleName=['week'])= event.date.dayOfWeek
              Span.day(styleName=['month'])= event.date.month
              Span.day(styleName=['number'])= event.date.dayOfMonth
            Div.content
              Span.text(styleName=['title'])= event.title
              Span.text(styleName=['orgs'])= event.orgs.join(' + ')
              Span.text(styleName=['time'])= event.date.startTime + ' - ' + event.date.endTime
            Button.icon(
              variant='text'
              size='l'
              icon=faChevronDown
              onPress=() => {}
            )
          Divider.divider
  `
})
