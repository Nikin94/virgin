import React from 'react'
import { observer } from 'startupjs'
import { Span, Div } from '@startupjs/ui'
import './index.styl'

export default observer(function HotelEvents ({ style, text = '' }) {
  return pug`
    Div.root(style=style)
      Span.text(styleName=['type'])= text
      Span.text(styleName=['title']) Events
      Div.line
  `
})
