import React from 'react'
import { observer } from 'startupjs'
import { Div, Button } from '@startupjs/ui'
import {
  faFacebookF,
  faInstagram,
  faLinkedin,
  faTwitter
} from '@fortawesome/free-brands-svg-icons'
import './index.styl'

export default observer(function Socials ({ style }) {
  const socials = [faFacebookF, faInstagram, faLinkedin, faTwitter]
  return pug`
    Div.root(style=style)
      each social, index in socials
        Button.social(key=index variant='text' icon=social onPress=() => {})
  `
})
