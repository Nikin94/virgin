import React from 'react'
import { observer } from 'startupjs'
import { Div, Span, TextInput, Button, Link } from '@startupjs/ui'
import './index.styl'

export default observer(function AddSuggestionForm ({ style }) {
  return pug`
    Div.root(style=style)
      Span.title Add a suggestion
      Span.subtitle We love to hear new ideas on how to be even more awesome
      TextInput.input(
        placeholder='Title of suggestion?'
      )
      TextInput.input(
        placeholder='Make a suggestion'
        numberOfLines=4
      )
      Div.bottom
        Button.btn(onPress=() => {})
          Span.btnText Post
        Div
          Span.description(variant='description') To send a private suggestion email us at:
          Link.link(to='#') culture@virginhotels.com
  `
})
