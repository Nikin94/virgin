import React, { useMemo } from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { Div } from '@startupjs/ui'
import './index.styl'

export default observer(function Logo ({ style, width, variant }) {
  const defaultWidth = 320
  const defaultHeight = 157

  const url = useMemo(() => {
    switch (variant) {
      case 'red':
        return 'img/main-logo-red.png'
      default:
        return 'img/main-logo-white.png'
    }
  }, [variant])

  const [actualWidth, actualHeight] = useMemo(() => {
    return [
      width || defaultWidth,
      width * defaultHeight / defaultWidth || defaultHeight
    ]
  }, [width])

  return pug`
    Div.root(style=style)
      Image(
        style={width: actualWidth, height: actualHeight}
        resizeMode='contain'
        source={uri: url}
      )
  `
})
