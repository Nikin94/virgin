import React, { useState } from 'react'
import { observer } from 'startupjs'
import { Div, Span, Button } from '@startupjs/ui'
import './index.styl'

export default observer(function Tabs ({ style }) {
  const tabs = ['all', 'most loved', 'in action', 'archive']
  const [activeTab, setActiveTab] = useState(tabs[0])
  return pug`
    Div.root(style=style)
      each tab, index in tabs
        Div.tab(key=tab)
          Button.button(
            variant='text'
            onPress=() => setActiveTab(tab)
          )
            Span.label= tab
          if tab === activeTab
            Div.triangle
          
  `
})
