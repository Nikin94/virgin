import React from 'react'
import { observer } from 'startupjs'
import { Div, Span, Row, Icon, Button } from '@startupjs/ui'
import Tabs from './Tabs'
import Replies from './Replies'
import suggestions from './suggestions.json'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import { faTimesCircle, faHeart } from '@fortawesome/free-regular-svg-icons'
import './index.styl'

export default observer(function SuggestionsList ({ style }) {
  return pug`
    Div.root(style=style)
      Tabs
      Div.suggestions
        each suggestion, index in suggestions
          Row.suggestion(key=index)
            Div.numberWrapper
              Span.number= suggestion.number
            Div.content
              Span.title= suggestion.title
              Row.nameWrapper(vAlign='end')
                Span.user= suggestion.userName
                Span.time(variant='description')= suggestion.createdAt
              Span.text= suggestion.text
              Row.actions(align='between')
                Row(vAlign='center')
                  Icon.button(icon=faCheck styleName=['icon'])
                  Span.action vote:
                  Span.action(variant='description' styleName=['count'])= suggestion.votes || 0
                Row(vAlign='center')
                  Button.button(icon=faTimesCircle variant='text' onPress=() => {})
                  Button.button(icon=faHeart variant='text' onPress=() => {})
                  Button.button(variant='text' onPress=() => {}) manage
              Replies.replies(replies=suggestion.replies)


              
  `
})
