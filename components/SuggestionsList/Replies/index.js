import React from 'react'
import { Image } from 'react-native'
import { observer } from 'startupjs'
import { Div, Row, Span, Button, TextInput, Avatar } from '@startupjs/ui'
import { faTimesCircle } from '@fortawesome/free-regular-svg-icons'
import './index.styl'

export default observer(function Replies ({ style, replies = [] }) {
  return pug`
    Div.root(style=style)
      each reply, index in replies
        Row.reply(key=index styleName={first: index === 0})
          Image.avatar(source={uri: reply.image})
          Row.container
            Div.content
              Span.name= reply.userName
              Span.text= reply.text
                if reply.hashtags
                  Span.hashtags
                    each hashtag in reply.hashtags
                      Span.hashtag(key=hashtag)= ' #' + hashtag
              Span.description(variant='description')= reply.createdAt
            Button.delete(
              icon=faTimesCircle
              variant='text'
              size='l'
              onPress=() => {}
            )
      Row.actions(styleName={marginTop: !!replies.length})
        Avatar(src='https://www.placecage.com/c/200/200' size='s')
        TextInput.input(
          placeholder='Reply to this suggestion...'
        )
  `
})
