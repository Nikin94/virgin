import React, { useMemo } from 'react'
import { observer, useValue } from 'startupjs'
import { Span, Menu as MenuUI } from '@startupjs/ui'
import menu from './menu.json'
import './index.styl'

export default observer(function Menu ({ style, compact, vertical }) {
  const [activeItem, $activeItem] = useValue(menu[0])

  const getLabel = item => {
    const prefix = item.addPerfix ? 'V ♥ ' : ''
    return pug`
      Span.label= prefix + item.label
    `
  }

  const filteredItems = useMemo(() => {
    return menu.filter(i => !compact || (compact && !i.disableOnSidebar))
  }, [compact, JSON.stringify(menu)])

  return pug`
    MenuUI.root(style=style styleName={vertical})
      each item, index in filteredItems
        MenuUI.Item.menuItem(
          styleName={vertical, first: index === 0}
          key=item.label
          active=activeItem.label === item.label
          onPress=() => $activeItem.set(item.label)
        )= getLabel(item) 
  `
})
